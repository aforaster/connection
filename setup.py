# -*- coding: utf-8 -*-
import setuptools

setuptools.setup(
    name="connection",
    author="Arnau Foraster",
    version="1.0.0",
    description="API project that displays relationships among two developers",
    packages=setuptools.find_packages(),
)
