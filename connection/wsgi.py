# -*- coding: utf-8 -*-
"""Module that defines a callable to be used by WSGI.

Also provide a minimal setup to start the flask API in a development environment.
"""
import os

from connection import create_app

flask_app = create_app()


def start(app) -> None:
    """Start a flask API with environment parameters if defined."""
    app.logger.info("Start development flask API")
    app.run(debug=os.environ.get('FLASK_DEBUG', 'True'),
            host=os.environ.get('HTTP_HOST', '0.0.0.0'),
            port=int(os.environ.get('HTTP_PORT', '5000')),
            )


if __name__ == "__main__":
    start(flask_app)
