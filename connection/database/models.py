# -*- coding: utf-8 -*-
"""Define database models used in the application."""
import datetime
import json

from marshmallow import Schema, fields, post_dump
from sqlalchemy import Boolean, Column, DateTime, Integer, JSON, String
from sqlalchemy.ext.declarative import declarative_base


class Base(object):
    """Class that holds common fields for all database models."""
    registered_at = Column(
        DateTime,
        default=datetime.datetime.utcnow,
        nullable=False,
    )
    updated_at = Column(
        DateTime,
        default=datetime.datetime.utcnow,
        onupdate=datetime.datetime.utcnow,
        nullable=False,
    )
    # This field is declared to detach destructive operations in DB.
    # Perform them nightly to not block tables
    is_deleted = Column(
        Boolean,
        default=False,
        nullable=False,
    )


DeclarativeBase = declarative_base(cls=Base)


class Connection(DeclarativeBase):
    """Model that defines the relationship information."""
    __tablename__ = 'connection'
    id = Column(Integer, primary_key=True)
    source_developer_name = Column(String, nullable=False)
    target_developer_name = Column(String, nullable=False)
    connected = Column(Boolean, default=False)
    organisations = Column(JSON)

    def __repr__(self):
        return f"<Connections ({self.source_developer_name} - {self.target_developer_name})"


class JsonField(fields.Field):
    """Field that serializes JSON and deserializes it."""
    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return ""
        return json.loads(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return json.dumps(value)


class ConnectionSchema(Schema):
    """Schema that converts a Connection object to desired API format."""
    class Meta:
        ordered = True

    # ordered fields that will be outputted
    registered_at = fields.DateTime(format="%Y-%m-%dT%H:%M:%S%z", strict=True)
    connected = fields.Bool(strict=True)
    organisations = JsonField(strict=True)

    @post_dump
    def remove_empty_orgs(self, data, **kwargs):
        # Don't display organisations field in output if they are not connected in twitter
        if not data['connected']:
            del data['organisations']
        # Don't display users as connected if they don't share organisations
        elif not data['organisations']:
            data['connected'] = False
            del data['organisations']
        return data
