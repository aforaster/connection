# -*- coding: utf-8 -*-
"""Helper module to retrieve and store data from database."""

from connection.database.models import Connection, ConnectionSchema


class ConnectionRepository:
    def __init__(self, db_session):
        self.session = db_session

    def save(self, instance) -> Connection:
        """Make passed instance persistent in database."""
        self.session.add(instance)
        self.session.commit()
        return instance

    def load(self, source_user: str, target_user: str) -> dict:
        """Query database and return ordered results."""
        connections = self.session.query(Connection).filter_by(
            source_developer_name=source_user,
            target_developer_name=target_user,
        ).order_by(Connection.registered_at.asc()).all()

        # many attribute is important since query can return multiple values
        return ConnectionSchema(many=True).dump(connections)
