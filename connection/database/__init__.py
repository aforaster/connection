# -*- coding: utf-8 -*-
"""Connect sqlalchemy session against dockerized postgres database."""
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


engine = create_engine("postgresql+psycopg2://{db_user}:{db_pass}@{db_host}/{db_name}".format(
    db_user=os.getenv("DB_USER", "test"),
    db_pass=os.getenv("DB_PASSWORD", "pwd12345"),
    db_host=os.getenv("DB_HOST", "0.0.0.0"),
    db_name=os.getenv("DB_NAME", "mydb"),
    )
)

Session = sessionmaker(bind=engine)

# create a Session instance that we will reuse globally
db_session = Session()
