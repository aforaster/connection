# -*- coding: utf-8 -*-
"""Define flask app creation.

Import routes namespace and hook it to flask app. Also init restplus plugin.
"""
from flask import Flask
from flask_restplus import Api

from connection.api import settings
from connection.api.routes import ns


def configure_app(flask_app: Flask) -> None:
    """Restplus config customization."""
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP
    flask_app.config['JSON_SORT_KEYS'] = settings.FLASK_SORT_JSON


def create_app() -> Flask:
    """Create a new Flask instance and wire restplus."""
    app = Flask(__name__)
    restplus_api = Api()
    restplus_api.add_namespace(ns)
    restplus_api.init_app(app)
    configure_app(app)
    return app
