# -*- coding: utf-8 -*-
"""Module that defines API endpoints.

Also creates a Manager instance that will control plugins usage.
"""
import logging

from flask import jsonify
from flask_restplus import Namespace, Resource

from connection.database import db_session
from connection.plugins import Manager
from connection.plugins.config import endpoints_configuration
from connection.plugins.github import GithubEndpoint
from connection.plugins.twitter import TwitterEndpoint

ns = Namespace('connected', description='Connected API endpoints')

# Define a manager and configure it
manager = Manager(
    github_class=GithubEndpoint(endpoints_configuration['github']),
    twitter_class=TwitterEndpoint(endpoints_configuration['twitter']),
    db_session=db_session,
)

log = logging.getLogger(__name__)


@ns.route('/realtime/<source_user>/<target_user>', methods=['GET'])
class Realtime(Resource):
    def get(self, source_user, target_user):
        """API realtime get endpoint."""
        log.debug(f"Realtime endpoint {source_user}, {target_user}")
        # names are sorted by convenience
        # make dev1 -> dev2 bidirectional
        source_name, target_name = sorted([source_user, target_user])
        result = manager.realtime(source_name, target_name)
        log.debug(f"Realtime endpoint {source_user}, {target_user}: {result}")
        return jsonify(result)


@ns.route('/register/<source_user>/<target_user>', methods=['GET'])
class Register(Resource):
    def get(self, source_user, target_user):
        """API register get endpoint."""
        log.debug(f"Register endpoint {source_user}, {target_user}")
        source_name, target_name = sorted([source_user, target_user])
        result = manager.register(source_name, target_name)
        log.debug(f"Register endpoint {source_user}, {target_user}: {result}")
        return jsonify(result)
