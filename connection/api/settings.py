# -*- coding: utf-8 -*-
"""Module that holds configuration options."""

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'full'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# Flask app settings
FLASK_SORT_JSON = False
