# -*- coding: utf-8 -*-
import unittest

import mock
import pytest
import tweepy

from connection.plugins import config
from connection.plugins.twitter import TwitterEndpoint


class TwitterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.result = dict()
        self.twitter_test = TwitterEndpoint({
            'consumer_key': 'consumer_key',
            'consumer_secret': 'consumer_secret',
            'access_token_key': 'access_token_key',
            'access_token_secret': 'access_token_secret',
        })

    @mock.patch('tweepy.API.get_user')
    def test_valid_user__exception(self, m_get_user):
        m_get_user.side_effect = tweepy.error.TweepError('user not found')
        result = self.twitter_test.valid_user('username')
        self.assertEqual(result, [f"username is no a valid user in twitter"])

    @mock.patch('tweepy.API.get_user')
    def test_valid_user_asd(self, m_get_user):
        result = self.twitter_test.valid_user('username')
        self.assertEqual(result, [])
        m_get_user.assert_called_once_with(screen_name='username')

    @mock.patch('tweepy.API.show_friendship', side_effect=[
        [mock.Mock(following=True, followed_by=True), mock.Mock()]
    ])
    def test_get_connections(self, m_get_followers):
        result = self.twitter_test.get_connections('source', 'target', self.result)
        self.assertDictEqual(result, self.result)
        m_get_followers.assert_called_once_with(source_screen_name='source',
                                                target_screen_name='target')

    @mock.patch('tweepy.API.show_friendship', side_effect=[
        [mock.Mock(following=True, followed_by=False), mock.Mock()]
    ])
    def test_get_connections__none(self, m_get_followers):
        result = self.twitter_test.get_connections('source', 'target', self.result)
        m_get_followers.assert_called_once_with(source_screen_name='source',
                                                target_screen_name='target')
        self.assertDictEqual(result, {'connected': False})


@pytest.mark.integration
class TwitterIntegrationTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.twitter = TwitterEndpoint(config.endpoints_configuration['twitter'])

    def test_valid_user__true(self):
        result = self.twitter.valid_user('pydanny')
        self.assertEqual(result, [])

    def test_valid_user__false(self):
        username = 'aagadsfalh123'
        result = self.twitter.valid_user(f'{username}')
        self.assertEqual(result, [f"{username} is no a valid user in twitter"])

    def test_get_connections__none(self):
        data = {}
        result = self.twitter.get_connections('324cat', 'pydanny', data)
        self.assertFalse(result['connected'])

    def test_get_connections__ok(self):
        data = {}
        result = self.twitter.get_connections('pydanny', 'audreyr', data)
        self.assertTrue(result['connected'])
