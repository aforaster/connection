# -*- coding: utf-8 -*-
import json

from connection.database.models import Connection
from connection.database.repository import ConnectionRepository
from connection.tests import factories
from connection.tests.factories import BaseTestCase


class RepositoryTestCase(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        factories.ConnectionFactory._meta.sqlalchemy_session = self.test_session
        self.connection_repo = ConnectionRepository(self.test_session)

    def test_save(self):
        instance = factories.ConnectionFactory.build()
        result = self.test_session.query(Connection).filter_by(id=instance.id).first()
        self.assertIsNone(result)
        self.connection_repo.save(instance)
        result = self.test_session.query(Connection).filter_by(id=instance.id).first()
        self.assertIsNotNone(result)

    def test_load(self):
        instance = factories.ConnectionFactory()
        factories.ConnectionFactory(source_developer_name=instance.source_developer_name,
                                    target_developer_name=instance.target_developer_name,
                                    connected=True)
        factories.ConnectionFactory(source_developer_name=instance.source_developer_name,
                                    target_developer_name=instance.target_developer_name,
                                    connected=True,
                                    organisations=json.dumps(['foo', 'bar']))

        results = self.connection_repo.load(instance.source_developer_name,
                                            instance.target_developer_name)
        self.assertEqual(len(results), 3)
