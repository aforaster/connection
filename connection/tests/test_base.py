# -*- coding: utf-8 -*-
import unittest

import mock

from connection.plugins.base import EndpointBase


class Dummy(EndpointBase):
    def get_connections(self, source_user: str, target_user: str) -> dict:
        pass

    def valid_user(self, user_id: str) -> bool:
        pass


class BaseTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.dummy = Dummy()
        self.result = dict(errors=[])

    @mock.patch('connection.tests.test_base.Dummy.valid_user')
    def test_validate_users__true_false(self, m_valid_user):
        m_valid_user.side_effect = [[], ["some error"]]
        result = self.dummy.validate_users('source', 'target', self.result)
        self.assertIsNotNone(result['errors'])

    @mock.patch('connection.tests.test_base.Dummy.valid_user')
    def test_validate_users__true_true(self, m_valid_user):
        m_valid_user.side_effect = [[], []]
        result = self.dummy.validate_users('source', 'target', self.result)
        self.assertTrue(result)
