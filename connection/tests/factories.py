# -*- coding: utf-8 -*-
import unittest

import factory
import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker

from connection.database import models
from connection.database.models import DeclarativeBase


class BaseTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """Create a database in memory for testing."""
        cls.engine = sqlalchemy.create_engine('sqlite:///:memory:',
                                              connect_args={'check_same_thread': False})
        cls.test_session = scoped_session(sessionmaker(bind=cls.engine))
        # Generate tables also
        DeclarativeBase.metadata.create_all(cls.engine)


class ConnectionFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Connection

    id = factory.Sequence(lambda n: n)
    source_developer_name = factory.Faker('name')
    target_developer_name = factory.Faker('name')
