# -*- coding: utf-8 -*-
"""Test API routes."""
import mock
from flask_testing import TestCase

from connection import create_app as connection_create_app


class RoutesTestCase(TestCase):
    def create_app(self):
        test_app = connection_create_app()
        test_app.config['TESTING'] = True
        return test_app

    @mock.patch('connection.api.routes.manager.realtime', return_value="")
    def test_realtime__order_names(self, m_realtime):
        response = self.client.get('/connected/realtime/foo/bar')
        m_realtime.assert_called_once_with('bar', 'foo')
        self.assert200(response)

    @mock.patch('connection.api.routes.manager.register', return_value="")
    def test_register(self, m_register):
        response = self.client.get('/connected/register/bar/foo')
        m_register.assert_called_once_with('bar', 'foo')
        self.assert200(response)
