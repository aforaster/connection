# -*- coding: utf-8 -*-
"""Test wsgi module."""
import unittest
from test.support import EnvironmentVarGuard

import mock

from connection import wsgi


class WsgiTestCase(unittest.TestCase):
    def test_default_start(self):
        app = mock.Mock()
        wsgi.start(app)
        app.run.assert_called_once_with(debug="True", host="0.0.0.0", port=5000)

    def test_set_environment_vars(self):
        self.env = EnvironmentVarGuard()
        self.env.set('FLASK_DEBUG', "False")
        self.env.set('HTTP_HOST', "0.0.0.1")
        self.env.set('HTTP_PORT', "1234")
        app = mock.Mock()
        with self.env:
            wsgi.start(app)
            app.run.assert_called_once_with(debug="False", host="0.0.0.1", port=1234)
