# -*- coding: utf-8 -*-
import unittest

import mock
import pytest
from github import UnknownObjectException

from connection.plugins import config
from connection.plugins.github import GithubEndpoint


class GithubTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.result = dict()

    @mock.patch('github.MainClass.Github.get_user')
    def test_valid_user__exception(self, m_get_user):
        m_get_user.side_effect = [UnknownObjectException(status='status', data='data')]
        github = GithubEndpoint({})
        result = github.valid_user('username')
        self.assertIsNotNone(result)

    @mock.patch('github.MainClass.Github.get_user')
    def test_valid_user(self, m_get_user):
        m_get_user.return_value = True
        github = GithubEndpoint({})
        result = github.valid_user('username')
        self.assertEqual(result, [])

    @mock.patch('github.MainClass.Github.get_user')
    def test_get_connections(self, m_get_users):
        m_get_users.return_value.get_orgs.side_effect = [[mock.Mock(login='foo')],
                                                         [mock.Mock(login='foo'),
                                                          mock.Mock(login='bar')]]
        github = GithubEndpoint({})
        result = github.get_connections('source', 'target', self.result)
        self.assertDictEqual(result, {'organisations': ['foo']})
        self.assertEqual(m_get_users.return_value.get_orgs.call_count, 2)

    @mock.patch('github.MainClass.Github.get_user')
    def test_get_connections__no_intersect(self, m_get_users):
        m_get_users.return_value.get_orgs.side_effect = [[mock.Mock(login='org')],
                                                         [mock.Mock(login='foo'),
                                                          mock.Mock(login='bar')]]
        github = GithubEndpoint({})
        result = github.get_connections('source', 'target', self.result)
        self.assertDictEqual(result, {'organisations': []})


@pytest.mark.integration
class GithubIntegrationTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.github = GithubEndpoint(config.endpoints_configuration['github'])

    def test_valid_user__true(self):
        result = self.github.valid_user('arnau-foraster')
        self.assertEqual(result, [])

    def test_valid_user__false(self):
        username = 'aagadsfalh123'
        result = self.github.valid_user(f'{username}')
        self.assertEqual(result, [f"{username} is no a valid user in github"])

    def test_get_connections__none(self):
        data = {}
        result = self.github.get_connections('arnau-foraster', 'pydanny', data)
        self.assertListEqual(result['organisations'], [])

    def test_get_connections__ok(self):
        data = {}
        result = self.github.get_connections('pydanny', 'audreyr', data)
        self.assertListEqual(sorted(result['organisations']),
                             sorted(['jazzband', 'cartwheelweb', 'twoscoops', 'opencomparison']))
