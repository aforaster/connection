# -*- coding: utf-8 -*-
import datetime
from collections import OrderedDict

import mock

from connection.plugins import Manager
from connection.tests.factories import BaseTestCase


class ManagerTestCase(BaseTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        self.manager = Manager(
            twitter_class=mock.Mock(),
            github_class=mock.Mock(),
            db_session=self.test_session,
        )

    @mock.patch('connection.database.repository.ConnectionRepository.save')
    def test_save(self, m_save) -> None:
        m_save.return_value = {'registered_at': datetime.datetime.now(),
                               'connected': False,
                               'organisations': "[]"}
        result = self.manager.save('foo', 'bar', {'connected': False, 'organisations': []})
        self.assertIsInstance(result, OrderedDict)
        m_save.assert_called_once()

    @mock.patch('connection.database.repository.ConnectionRepository.load')
    def test_register(self, m_load):
        self.manager.register('foo', 'bar')
        m_load.assert_called_once_with('foo', 'bar')

    def test_realtime__errors(self):
        twitter_klass = mock.Mock()
        twitter_klass.validate_users.return_value = {'errors': ["dev1 is no a valid user in twitter",
                                                                "dev2 is no a valid user in twitter"]}
        github_klass = mock.Mock()
        github_klass.validate_users.return_value = {'errors': ["dev1 is no a valid user in twitter",
                                                               "dev2 is no a valid user in twitter",
                                                               "dev1 is no a valid user in github"]}
        self.manager.validate = [twitter_klass.validate_users,
                                 github_klass.validate_users]
        result = self.manager.realtime('foo_user', 'bar_user')
        self.assertIsInstance(result, dict)
        self.assertDictEqual(result, {'errors': ["dev1 is no a valid user in twitter",
                                                 "dev2 is no a valid user in twitter",
                                                 "dev1 is no a valid user in github"]})
