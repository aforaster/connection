# -*- coding: utf-8 -*-
"""Class that controls registered plugins."""
import json
import logging

from connection.database.models import Connection, ConnectionSchema
from connection.database.repository import ConnectionRepository

log = logging.getLogger(__name__)


class Manager:
    def __init__(self, twitter_class, github_class, db_session):
        self.twitter = twitter_class
        self.github = github_class
        # define a list of methods that will run to validate input data
        self.validate = [
            self.twitter.validate_users,
            self.github.validate_users,
        ]
        # define a list of methods that will run to extract_data from them
        self.data_extraction = [
            self.twitter.get_connections,
            self.github.get_connections,
        ]
        self.db_session = db_session
        log.info(f"Manager init finished")

    def realtime(self, source_user: str, target_user: str) -> dict:
        result = dict(errors=[])
        for method in self.validate:
            result = method(source_user, target_user, result)
        if len(result['errors']):
            return result

        result = dict()
        for method in self.data_extraction:
            result = method(source_user, target_user, result)

        log.info(f"Save to database: {source_user}, {target_user}, {result}")
        return self.save(source_user, target_user, result)

    def register(self, source_user: str, target_user: str) -> dict:
        log.info(f"Load data of {source_user}, {target_user}")
        connections = ConnectionRepository(db_session=self.db_session).load(source_user, target_user)
        return connections

    def save(self, source_user: str, target_user: str, results: dict) -> dict:
        connection = Connection(
            source_developer_name=source_user,
            target_developer_name=target_user,
            connected=results.get('connected'),
            organisations=json.dumps(results.get('organisations'))
        )
        repository = ConnectionRepository(self.db_session)
        saved_instance = repository.save(connection)
        return ConnectionSchema().dump(saved_instance)
