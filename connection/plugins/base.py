# -*- coding: utf-8 -*-
"""Abstract class definition of plugins.

Define a model class that child classes will use. By doing this
we produce similar classes therefore we increase extensibility
and maintainability of our code.
"""
import abc


class EndpointBase(abc.ABC):
    @abc.abstractmethod
    def get_connections(self, source_user: str, target_user: str, result: dict) -> dict:
        """Function that contains the logic for each plugin.

        Should writte to result dict it's own data.
        """
        pass

    @abc.abstractmethod
    def valid_user(self, user_id: str) -> list:
        """Function that validates an username.

        Each plugin has to write it's own detail implementation.
        Should return [] if user is valid, otherwise must return [error message]
        """
        pass

    def validate_users(self, source_user: str, target_user: str, result: dict) -> dict:
        """Function that checks if both developer are valid."""
        result['errors'].extend(self.valid_user(source_user))
        result['errors'].extend(self.valid_user(target_user))
        return result
