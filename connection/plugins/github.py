# -*- coding: utf-8 -*-
"""Github plugin implementation detail."""
import logging

from github import Github, UnknownObjectException

from connection.plugins.base import EndpointBase

log = logging.getLogger(__name__)


class GithubEndpoint(EndpointBase):
    def __init__(self, parameters):
        log.debug(f"GithubEndpoint.__init__({parameters})")
        self.github = Github(**parameters)

    def get_connections(self, source_user: str, target_user: str, result: dict) -> dict:
        """Write to result dict organizations in common between both users."""
        log.debug(f"GithubEndpoint.get_connections({source_user}, {target_user})")
        # create two sets and use intersection, faster than list lookup
        source_user_orgs = set([organization.login for organization in self.github.get_user(source_user).get_orgs()])
        target_user_orgs = set([organization.login for organization in self.github.get_user(target_user).get_orgs()])

        orgs = list(source_user_orgs.intersection(target_user_orgs))
        log.debug(f"GithubEndpoint {source_user}, {target_user} share: {orgs}")
        result['organisations'] = orgs
        return result

    def valid_user(self, username: str) -> list:
        """Check if an username is valid against github API.

        Return [] if username is valid, otherwise return [error message].
        """
        valid = []
        try:
            self.github.get_user(username)
        except UnknownObjectException:
            valid = [f"{username} is no a valid user in github"]
        return valid
