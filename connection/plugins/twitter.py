# -*- coding: utf-8 -*-
"""Twitter plugin implementation detail."""
import logging

import tweepy

from connection.plugins.base import EndpointBase

log = logging.getLogger(__name__)


class TwitterEndpoint(EndpointBase):
    def __init__(self, parameters):
        log.debug(f"TwitterEndpoint.__init__({parameters})")
        auth = tweepy.OAuthHandler(parameters['consumer_key'], parameters['consumer_secret'])
        auth.set_access_token(parameters['access_token_key'], parameters['access_token_secret'])

        self.twitter = tweepy.API(auth)

    def get_connections(self, source_user: str, target_user: str, result: dict) -> dict:
        """Function that searches if two users are connected in Twitter.

        Write down connected = True in results dict otherwise write False
        """
        log.debug(f"TwitterEndpoint.get_connections({source_user}, {target_user})")
        result['connected'] = False
        friendship_source, friendship_target = self.twitter.show_friendship(source_screen_name=source_user,
                                                                            target_screen_name=target_user)

        # Check one side of relationship
        if friendship_source.following and friendship_source.followed_by:
            log.debug(f"TwitterEndpoint {source_user}, {target_user} are connected.")
            result['connected'] = True
        return result

    def valid_user(self, username: str) -> list:
        """Check if an username is valid against twitter API.

        Return [] if username is valid, otherwise return [error message].
        """
        valid = []
        try:
            self.twitter.get_user(screen_name=username)
        except tweepy.error.TweepError:
            valid = [f"{username} is no a valid user in twitter"]
        return valid
