#Introduction 
The aim of this project is create an API with two endpoints that returns information about 
the relationship of two developers. 

#Production environment (start here for testing the API)
1.  If you want to start production API and related containers you can simply:
    ```
    docker-compose up
    ```
2.  Once the containers are up and running we need to execute migration script:
    ```
    docker-compose run api alembic upgrade head
    ```
3.  Then you will be able to enter the API in url: http://0.0.0.0/

4.  Swagger will display the available endpoints, feel free to press "Try it out"
    button and then fill the two names in the form. After that press "Execute" button.

5. After some seconds the response body will appear.
 
#Tests, code style and complexity metrics
Tox is a self contained tool that simplifies the execution of all this checks from a single point.
1. Activate a virtualenv and install
   ```
   pip install tox~=3.14.1
   ```
2. You can run tox, from project root (code style check, cyclomatic complexity and unit tests) with the command:
   ```
   tox
   ```

3. Integration tests run a part:
   ```
   tox -e integration
   ```

#Create development environment
1.	Generate a virtualenv and install dependencies with:
    ```pip -r requirements-dev.txt```
    
2.  You can run development API by running with python wsgi.py module
3.  Open a web browser with url: http://0.0.0.0:5000/
   
#FUTURE IMPROVEMENTS
- Database normalization 
- If speed-up needed, multithreading endpoint calls
